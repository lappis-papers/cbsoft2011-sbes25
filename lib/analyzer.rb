require 'fileutils'
require 'digest/md5'

class Analyzer

  MATCHERS = [
    /software\s*livre/im,
    /softwares\s*livres/im,
    /ferramenta\s*livre/im,
    /ferramentas\s*livres/im,
    /ferramenta\s*aberta/im,
    /ferramentas\s*abertas/im,
    /código\s*aberto/im,
    /odigo\s*aberto/im,   # HACK for problem with accents in PDF's
    /digo\s*aberto/im,    # HACK for problem with accents in PDF's
    /free\s*software/im,
    /open\s*source/im,
    /software\s*aberto/im,
    /softwares\s*abertos/im,
    /open\s*software/im,
    /libre\s*software/im,
    /\bOSS\b/im,
    /\bFLOSS\b/im,
    /\bFOSS\b/im,
    /\bOSSD\b/im,
    /software\s*repository/im,
    /software\s*repositories/im,
    /repositório\s*de\s*software/im,
    /repositórios\s*de\s*software/im,
    /repositorio\s*de\s*software/im,
    /repositorios\s*de\s*software/im,
  ]

  TMPDIR = File.join('/tmp', 'sbes25-' + (ENV['USER'] || ENV['USERNAME'] || 'unknown-user'))
  FileUtils.mkdir_p TMPDIR

  def text_for(pdf)
    cache_file = File.join(TMPDIR, pdf.gsub('.pdf', '.txt'))
    if !File.exists?(cache_file)
      FileUtils.mkdir_p(File.dirname(cache_file))
      system("pdftotext", pdf, cache_file)
      if $?.exitstatus != 0
        $stderr.puts "W: pdftotext failed on #{pdf} !!!"
      end
      FileUtils.rm_f '/tmp/tmp.ps'
      File.open(File.join(TMPDIR, 'mapping.txt'), 'a') do |file|
        file.puts "#{cache_file} -> #{pdf}"
      end
    end
    File.read(cache_file)
  end

  def matches?(pdf)
    text = text_for(pdf)
    MATCHERS.any? { |r| text =~ r }
  end

end

# FIXME move to another place
if $PROGRAM_NAME == __FILE__
  def output(file, *data)
    file.puts data.join(',')
  end
  for track in %w[maintrack tools]
    File.open(track + '.csv', 'w') do |file|
      output(file, 'year', 'file', 'mentions')
      Dir.glob('data/' + track + '/*').sort.select { |d| File.directory?(d) }.each do |dir|
        year = File.basename(dir)
        Dir.glob("#{dir}/*.{pdf,PDF}").each do |pdf|
          analyzer = Analyzer.new
          mentions = analyzer.matches?(pdf) ? 1 : 0
          output(file, year, pdf, mentions)
        end
      end
    end
  end
end
