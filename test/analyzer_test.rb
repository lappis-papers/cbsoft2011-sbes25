require 'test/unit'
require 'shoulda'
require 'mocha'

require 'analyzer'

class AnalyzerTest < Test::Unit::TestCase

  def setup
    @analyzer = Analyzer.new
  end

  attr_reader :analyzer

  should 'search for exact expressions' do
    analyzer.stubs(:text_for).returns(
      'this article looks at free software projects for ...',
      'this article is about proprietary software that is free as in beer'
    )
    assert analyzer.matches?('1.pdf')
    assert !analyzer.matches?('2.pdf')
  end

  should 'pick expression in separate lines' do
    analyzer.stubs(:text_for).returns("this article looks at free\nsoftware projects for ...")
    assert analyzer.matches?('the.pdf')
  end

  should 'be able to extract at least non-accented text from PDF files in portuguese' do
    assert analyzer.matches?('test/test.pdf')
  end

  should 'match acronyms' do
    analyzer.stubs(:text_for).returns(
      'Five FLOSS projects',
      'Several OSS projects'
    )
    assert analyzer.matches?("1.pdf")
    assert analyzer.matches?('2.pdf')
  end

  should 'not match acronyms inside other words' do
    analyzer.stubs(:text_for).returns('I should loss at least 10 quilograms by the end of the year')
    assert !analyzer.matches?("1.pdf")
  end

end
